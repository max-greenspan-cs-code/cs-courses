#
# board.py (Final project)
#
# A Board class for the Eight Puzzle
#
# name: Max Greenspan
# email: maxgspan@bu.edu

# a 2-D list that corresponds to the tiles in the goal state
GOAL_TILES = [['0', '1', '2'],
              ['3', '4', '5'],
              ['6', '7', '8']]


class Board:
    """ A class for objects that represent an Eight Puzzle board.
    """

    def __init__(self, digitstr):
        """ a constructor for a Board object whose configuration
            is specified by the input digitstr
            input: digitstr is a permutation of the digits 0-9
        """
        # check that digitstr is 9-character string
        # containing all digits from 0-9
        assert (len(digitstr) == 9)
        for x in range(9):
            assert (str(x) in digitstr)

        self.tiles = [[''] * 3 for x in range(3)]
        self.blank_r = -1
        self.blank_c = -1


        for r in range(len(self.tiles)):
            for c in range(len(self.tiles[0])):
                num = digitstr[3 * r + c]
                self.tiles[r][c] = num
                if num == '0':
                    self.blank_r = r
                    self.blank_c = c


    def __repr__(self):
        """ returns a string representation of a Board object
        """
        s = ""
        for r in range(len(self.tiles)):
            for c in range(len(self.tiles[0])):
                if self.tiles[r][c] != "0":
                    s += self.tiles[r][c] + " "
                else:
                    s += "_ "
            s += '\n'
        return s

    def move_blank(self, direction):
        """ takes as input a string direction that specifies the direction in which the blank should move
        """
        new_row = self.blank_r
        new_col = self.blank_c

        if direction == 'up':
            new_row -= 1
        elif direction == 'down':
            new_row += 1
        elif direction == 'left':
            new_col -= 1
        elif direction == 'right':
            new_col += 1
        else:
            return False

        if not (0 <= new_row <= 2 and 0 <= new_col <= 2):
            return False
        else:
            prev_element = self.tiles[new_row][new_col]
            self.tiles[new_row][new_col] = '0'
            self.tiles[self.blank_r][self.blank_c] = prev_element
            self.blank_r = new_row
            self.blank_c = new_col
            return True

    def digit_string(self):
        """ creates and returns a string of digits that corresponds to the current
            contents of the called Board object’s tiles attribute
        """
        s = ''
        tiles = self.tiles
        for r in range(len(tiles)):
            for c in range(len(tiles)):
                s += tiles[r][c]
        return s

    def copy(self):
        """ returns a newly-constructed Board object that is a deep copy of the called object
        """
        digit_string = self.digit_string()
        return Board(digit_string)

    def num_misplaced(self):
        """ counts and returns the number of tiles in the called Board
            object that are not where they should be in the goal state
        """
        ctr = 0
        tiles = self.tiles
        for r in range(len(tiles)):
            for c in range(len(tiles)):
                if tiles[r][c] != "0":
                    if tiles[r][c] != GOAL_TILES[r][c]:
                        ctr += 1
        return ctr

    def move_misplaced(self):
        """ Get the sum of the minimum moves that should be made
            to get the misplaced pieces in the right position
        """
        moves = 0
        current_r = -1
        current_c = -1
        goal_r = -1
        goal_c = -1

        for n in range(1, 8):
            for r in range(len(self.tiles)):
                for c in range(len(self.tiles[0])):
                    if self.tiles[r][c] == str(n):
                        current_r = r
                        current_c = c

            if GOAL_TILES[r][c] != str(n):
                for r in range(len(self.tiles)):
                    for c in range(len(self.tiles[0])):
                        if GOAL_TILES[r][c] == str(n):
                            goal_r = r
                            goal_c = c
            moves += abs(current_r - goal_r) + abs(current_c - goal_c)
        return moves

    def __eq__(self, other):
        """ should return True if the called object (self) and the argument
            (other) have the same values for the tiles attribute
        """
        if self.tiles == other:
            return True
        else:
            return False





