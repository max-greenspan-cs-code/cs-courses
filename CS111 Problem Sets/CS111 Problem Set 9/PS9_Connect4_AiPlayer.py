#
# ps9pr4.py (Problem Set 9, Problem 4)
#
# AI Player for use in Connect Four  
#

import random  
from PS9_Connect4_Client import *

class AIPlayer(Player):
    """ A Class that uses AI to lookahead
     and predict moves as a connect 4 player"""

    def __init__(self, checker, tiebreak, lookahead):
        """ Initializes the AI player function"""
        assert(checker == 'X' or checker == 'O')
        assert(tiebreak == 'LEFT' or tiebreak == 'RIGHT' or tiebreak == 'RANDOM')
        assert(lookahead >= 0)

        super().__init__(checker)
        self.tiebreak = tiebreak
        self.lookahead = lookahead

    def __repr__(self):
        """ Returns a string representation of the object, 
        its checker, its tiebreak decision, and its lookahead"""
        return('Player ' + self.checker + ' (' + self.tiebreak + ', ' + str(self.lookahead) + ')')

    def max_score_column(self, scores):
        """ determines the maximum score 
        column depending on tiebreak rule """
        opts = []
        greatest = max(scores)
        for i in range(len(scores)):
            if scores[i] == greatest:
                opts += [i]
        if self.tiebreak == 'LEFT':
            return opts[0]
        elif self.tiebreak == 'RIGHT':
            return opts[-1]
        elif self.tiebreak == 'RANDOM':
            return random.choice(opts)

    def scores_for(self, b):
        """ Uses AI lookahead to determine the
        scores for each respective checker position """
        scores = [50] * b.width
        for col in range(b.width):
            if b.can_add_to(col) == False:
                scores[col] = -1
            elif b.is_win_for(self.checker):
                scores[col] = 100
            elif b.is_win_for(self.opponent_checker()):
                scores[col] = 0 
            elif self.lookahead == 0:
                scores[col] = 50
            else:
                b.add_checker(self.checker, col)
                other = AIPlayer(self.opponent_checker(), self.tiebreak, self.lookahead-1)
                opp_scores = other.scores_for(b)
                opp_col = other.max_score_column(opp_scores)
                
                if opp_scores[opp_col] == 100:
                    scores[col] = 0
                elif opp_scores[opp_col] == 0:
                    scores[col] = 100
                elif opp_scores[opp_col] == 50:
                    scores[col] = 50
                b.remove_checker(col)
        return scores

    def next_move(self, b):
        """ Uses AI lookahead to determine the best next move """
        best = self.scores_for(b)
        self.num_moves +=1
        return self.max_score_column(best)

