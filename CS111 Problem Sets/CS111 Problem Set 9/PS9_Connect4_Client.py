#
# ps9pr3.py  (Problem Set 9, Problem 3)
#
# Playing the game 
#   

from PS9_Connect4_Board import Board
from PS9_Connect4_Player import Player
import random
    
def connect_four(p1, p2):
    """ Plays a game of Connect Four between the two specified players,
        and returns the Board object as it looks at the end of the game.
        inputs: p1 and p2 are objects representing Connect Four
          players (objects of the class Player or a subclass of Player).
          One player should use 'X' checkers and the other player should
          use 'O' checkers.
    """
    if p1.checker not in 'XO' or p2.checker not in 'XO' \
       or p1.checker == p2.checker:
        print('need one X player and one O player.')
        return None

    print('Welcome to Connect Four!')
    print()
    b = Board(6, 7)
    print(b)
    
    while True:
        if process_move(p1, b) == True:
            return b

        if process_move(p2, b) == True:
            return b

def process_move(p, b):
    """ processes a move and displays
     the board and outcome"""
    name = str(p)
    print(name + "'s turn")
    
    col_choice = p.next_move(b)
    b.add_checker(p.checker, col_choice)
    print()
    print(b)
    
    if b.is_win_for(p.checker):
        print(name + ' wins in ' + str(p.num_moves) + ' moves.\nCongratulations!')
        return True
    elif b.is_full():
        print("It's a tie!")
        return True
    else:
        return False

class RandomPlayer(Player):
    """ Creates a RandomPlayer class
     that plays random moves"""
    def next_move(self, b):
        """ Determines the next move at 
        random under the RandomPlayer class"""
        s = []
        for i in range(b.width):
            if b.can_add_to(i):
                s += [i]
        self.num_moves += 1
        return random.choice(s)
