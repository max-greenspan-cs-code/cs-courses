#
# ps9pr2.py (Problem Set 9, Problem 2)
#
# A Connect-Four Player class 
#  

from PS9_Connect4_Board import Board


class Player:
    """ A data type for connect 4 
    that represents a given player"""
    def __init__(self, checker):
        """ initializes the object and checker"""
        assert(checker == 'X' or checker == 'O')
        
        self.checker = checker
        self.num_moves = 0
        
    def __repr__(self):
        """ Returns a string that
        represents a players checker"""
        return 'Player ' + self.checker
    
    def opponent_checker(self):
        """ Returns a string that
        represents the opponents checker"""
        if self.checker == 'X':
            return 'O'
        elif self.checker == 'O':
            return 'X'
        else:
            return 'Error'
        
    def next_move(self, b):
        """ Asks the player to select a column to place
        a checeker in and also tracks the number of moves"""
        while True:
            user = int(input('Enter a column: '))
            if b.can_add_to(user) == True:
                self.num_moves += 1
                return user
            else:
                print('Try again!')
            