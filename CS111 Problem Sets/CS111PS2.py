#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 14:50:23 2021

@author: maxgreenspan
"""
# 
# ps2pr3.py - Problem Set 2
#
# Indexing and slicing puzzles
#

def longer(s1, s2):
    """ takes two inputs, outputs the longer 
    length input, or the first one if equal length """
    if len(s1) >= len(s2):
        return s1
    else:
        return s2

def swap_halves(s):
    """takes input s and returns first half of s as the second half and vice
    versa, if gdd the first character should have 
    on fewer character than the second half"""
    half = len(s) // 2
    if len(s) % 2 == 0:
        return s[half:] + s[:half]
    else:
        return s[half:] + s[:half]

def repeat_one(values, index, num_times):
    """inputs a list of values of a list, an index, and a num_times. 
    returns a list where element at position index is repeated by num_times"""
    return values[0:index] + ([values[index]] * num_times) + values[1 + index:]


# The code below tests the values of your expressions. DO NOT MODIFY IT!
if __name__ == '__main__':    
    for n in range(0, 9):
        answer_var = 'answer' + str(n)
        if answer_var in dir():
            print(answer_var, '=', eval(answer_var))




def mult(vals):
    """inputs a list of values and uses recursion to 
    multiply them all together and return the product"""
    if len(vals) == 0:
        return 1
    else :
        vals_rest = mult(vals[1:])
        if vals[0] != 0 :
            return vals[0] * vals_rest
        else:
            return 0
        

def add_stars(s):
    """inputs string s and returns a star between each pair of adjacent characters
    if input is 2 or less characters, the function doesnt add stars"""
    if len(s) == 1 :
        return  s
    else :
        s_rest = add_stars(s[1:])
        return s[0] + '*' + s_rest
            
        
        

def dot(vals1, vals2):
    """ inputs two values and returns their dot product """
    if vals1 == [] or vals2 == [] or len(vals1) != len(vals2):
        return 0.0
    else :
        dot_storage = dot(vals1[1:], vals2[1:])
        return dot_storage + (vals1[0] * vals2[0])
    
def any_odd(vals):
    """ inputs a list of values and returns a boolean 
    true if there are any odd values"""
    if vals == []:
        return False
    else :
        val_storage = any_odd(vals[1:])                     
        if vals[0] % 2 == 1 :
            return True
        else:
            return val_storage
        
def process(vals):
    """ inputs a list and returns the same list but 
    with the square of each odd element"""
    if vals == []:
        return vals
    else :
        prc_storage = process(vals[1:])
        if vals[0] % 2 == 1:
            return [vals[0] * vals[0]] + prc_storage
        else :
            return [vals[0]] + prc_storage



def letter_score(letter):
    """ inputs letter, returns scrabble value for that 
    lowercase letter, otherwise returns 0"""
    assert(len(letter) == 1)
    if letter in ['a', 'e', 'i', 'l', 'n', 'o', 'r', 's', 't', 'u']:
        return 1
    elif letter in ['d', 'g'] :
        return 2
    elif letter in ['b', 'c', 'm', 'p']:
        return 3
    elif letter in ['f', 'h', 'v', 'w', 'y']:
        return 4
    elif letter in ['k']:
        return 5
    elif letter in ['j', 'x']:
        return 8
    elif letter in ['q', 'z']:
        return 10
    else :
        return 0

def scrabble_score(word) :
    """ inputs string, returns the scrabble score of that string """
    if len(word) == 0:
        return 0
    else :
        current_score = scrabble_score(word[1:])
        return letter_score(word[0]) + current_score
    

def compare(s1, s2):
    """ inputs two strings and returns the number 
    of characters where they have discrepencies"""
    if s1 == '':
        return 0
    else :
        current_length = compare(s1[1:], s2[1:])
        if s1[0] == s2[0]:
            return current_length
        else :
            return 1 + current_length

    
def weave(vals1, vals2):
    """ inputs two lists and weaves them together """
    if len(list(vals1)) == 0 or len(list(vals2)) == 0:
        return vals1 + vals2
    else :
        vals_current = weave(vals1[1:], vals2[1:])
        return [vals1[0]] + [vals2[0]] + vals_current
    
    
    
