#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 17:25:14 2021

@author: maxgreenspan
"""

def abs_list_lc(values):
    """ inputs a list of values and returns the absolute value """
    return [abs(x) for x in values]

def abs_list_rec(values) :
    """ inputs a list of values and returns the absolute value using recursion"""
    if values == []:
        return []
    else:
        values_srg = abs_list_rec(values[1:])
        return [abs(values[0])] + values_srg

def num_vowels(s):
    """ returns the number of vowels in the string s if values[0] % m == 0
        input: s is a string of 0 or more lowercase letters
    """
    if s == '':
        return 0
    else:
        num_in_rest = num_vowels(s[1:])
        if s[0] in 'aeiou':
            return 1 + num_in_rest
        else:
            return 0 + num_in_rest

def most_vowels(words):
    """ inputs a list of strings and returns 
    the string with the most vowels """
    vowel_list = [[num_vowels(x), x] for x in words]
    return max(vowel_list)[1]

def num_multiples(m, values):
    """ inputs an integer m and a list of integers values and 
    returns the number of integers in values that are multiples of m """
    num_actual = [x for x in values if x % m == 0]
    return len(num_actual)

def begins_with(prefix, wordlist):
    """inputs a string prefix and a string list and returns 
    all words from wordlist that begin with prefix """
    return [x for x in wordlist if x[:len(prefix)] == prefix] 


# Problem 3:


def rot(c, n):
    """ rotates c by n letters, returns c if c isn't a letter"""
    # Put the rest of your code for this function below.   
    if 'a' <= c <= 'z':
        new_ord = ord(c) + n
        if new_ord > ord('z'):
            new_ord = ord(c) + n - 26
    elif 'A' <= c <= 'Z':
        new_ord = ord(c) + n
        if new_ord > ord('Z'):
            new_ord = ord(c) + n -26
    else :
        return c
    return chr(new_ord)
    # check to ensure that c is a single character
    assert(type(c) == str and len(c) == 1)


def encipher(s, n):
    """ takes input s and rotates it by n using rot function """
    if s == '' :
        return ''
    else :
        current = encipher(s[1:], n)
        return rot(s[0], n) + current
    
    
def letter_prob(c):
    """ if c is the space character (' ') or an alphabetic character,
        returns c's monogram probability (for English);
        returns 1.0 for any other character.
        adapted from:
        http://www.cs.chalmers.se/Cs/Grundutb/Kurser/krypto/en_stat.html
    """
    # check to ensure that c is a single character   
    assert(type(c) == str and len(c) == 1)

    if c == ' ': return 0.1904
    if c == 'e' or c == 'E': return 0.1017
    if c == 't' or c == 'T': return 0.0737
    if c == 'a' or c == 'A': return 0.0661
    if c == 'o' or c == 'O': return 0.0610
    if c == 'i' or c == 'I': return 0.0562
    if c == 'n' or c == 'N': return 0.0557
    if c == 'h' or c == 'H': return 0.0542
    if c == 's' or c == 'S': return 0.0508
    if c == 'r' or c == 'R': return 0.0458
    if c == 'd' or c == 'D': return 0.0369
    if c == 'l' or c == 'L': return 0.0325
    if c == 'u' or c == 'U': return 0.0228
    if c == 'm' or c == 'M': return 0.0205
    if c == 'c' or c == 'C': return 0.0192
    if c == 'w' or c == 'W': return 0.0190
    if c == 'f' or c == 'F': return 0.0175
    if c == 'y' or c == 'Y': return 0.0165
    if c == 'g' or c == 'G': return 0.0161
    if c == 'p' or c == 'P': return 0.0131
    if c == 'b' or c == 'B': return 0.0115
    if c == 'v' or c == 'V': return 0.0088
    if c == 'k' or c == 'K': return 0.0066
    if c == 'x' or c == 'X': return 0.0014
    if c == 'j' or c == 'J': return 0.0008
    if c == 'q' or c == 'Q': return 0.0008
    if c == 'z' or c == 'Z': return 0.0005 
    return 1.0

# Code for the decipher function below. 

def list_options(s):
    """ input string s and give the 26 options it could be enciphered as """
    lst = [encipher(s, num) for num in range(26)]
    return lst

def word_score(options):
    """ inputs the 26 string options and creates a list of each score"""
    if options == '':
        return 0
    else :
        current_option = word_score(options[1:])
        return letter_prob(options[0]) + current_option 

def score_and_list(l):
    """ Takes list_options, and applies word_score to
    each, and returns the [score, list option] """
    scored_phrases = [[word_score(x), x] for x in l]
    best_phrase = max(scored_phrases)
    return best_phrase[1]

# use the helper function list_options to get the 26 options, 
# then use that to get a list of the possible scores, and then
# use score_and_list to determine the best score and return that string, 
# and use decipher to facilitate all of this

def decipher(s):
    """ inputs string s and applies list_options function to it,
    then it applies score_and_list function to the result of list_options, 
    and returns the string with the highest score"""
    a = list_options(s)
    b = score_and_list(a)
    return b

# Problem 4:

def index(elem, seq):
    """ inputs elem and seq, and returns the 
    index of the first occurence of elem in seq """
    if len(seq) == 0:
        return -1
    elif elem == seq[0]:
        return 0
    else :
        storage = index(elem, seq[1:])
        if storage == -1:
            return -1
        else:
            return 1 + storage

def index_last(elem, seq):
    """ inputs elem and seq, and returns
    the index of the last occurence of elem in seq """
    a = len(seq) -1
    if len(seq) == 0:
        return -1
    elif elem == seq[-1]:
        return a
    else:
        storage = index_last(elem, seq[:-1])
        a = len(seq)
        if storage == -1:
            return -1
        return  storage 

#helper function for jotto score 
def rem_first(elem, values):
    """ inputs elem and values and 
    removes the first occurence of elem in values
    """
    if values == '':
        return ''
    elif values[0] == elem:
        return values[1:]
    else:
        result_rest = rem_first(elem, values[1:])
        return values[0] + result_rest


def jscore(s1, s2):
    """ inputs s1 and s2 and returns the amount of overlapping 
    letters, or the jotto score """
    if s1 == '' or s2 == '':
        return 0
    else:
        str1_score = rem_first(s1[0],s1)
        str2_score = rem_first(s1[0],s2)
        if s1[0] in s2:
            return 1 + jscore(str1_score ,str2_score)
        else :
            return 0 + jscore(s1[1:], s2)
        
