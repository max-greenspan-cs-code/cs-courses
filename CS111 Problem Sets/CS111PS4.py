#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 16:52:06 2021

@author: maxgreenspan
"""

def dec_to_bin(n):
    """ inputs a decimal number and returns it in binary"""
    if n == 0:
        return '0'
    elif n == 1:
        return '1'
    else :
        dec_rest = dec_to_bin(n//2)
        if n % 2 == 0:
            return dec_rest + '0'
        elif n % 2 != 0:
            return dec_rest + '1'
        

def bin_to_dec(b):
    """ inputs a binary number and returns it in decimal """
    if b == '0':
        return 0
    elif b == '1':
        return 1
    else :
        bin_rest = bin_to_dec(b[:-1])
        if b[-1] == '1':
            return 2 * bin_rest + 1
        else :
            return 2 * bin_rest

# Problem 2:

def mul_bin(b1, b2):
    """ inputs 2 binary strings and outputs the product """
    n1 = bin_to_dec(b1)
    n2 = bin_to_dec(b2)
    transfer = n1 * n2
    return dec_to_bin(transfer)

def largest_bin(binvals):
    """ inputs a list of binary strings and returns the largest element """
    opts = [[bin_to_dec(x), x] for x in binvals]
    best_opts = max(opts)
    return best_opts[1]

# Problem 3:

def count_evens_rec(binvals):
    """ uses recursion on a list of binvals and returns the number of even inputs """
    if binvals == []:
        return 0
    else :
        ev_rest = count_evens_rec(binvals[1:])
        list_bin = binvals[0]
        if list_bin[-1] == '0':
            return ev_rest + 1
        else :
            return ev_rest + 0

def count_evens_lc(binvals):
    """ uses list comprehension on a list of binvals and returns the number of even inputs """
    num_evens = [x for x in binvals if x[-1] == '0']
    return len(num_evens)

def add_bitwise(b1, b2):
    """ inputs two binary numbers and adds them together """
    if b1 == '' and b2 == '':
        return ''
    elif b1 == '':
        return b2
    elif b2 == '':
        return b1
    else :
        bit_str = add_bitwise(b1[:-1], b2[:-1])
        if b1[-1] == '0' and b2[-1] == '0':
            return bit_str + '0'
        elif b1[-1] == '0' and b2[-1] == '1' or b1[-1] == '1' and b2[-1] == '0':
            return bit_str + '1'
        else :
            bit_str = add_bitwise(b1[:-1] + '1',b2[:-1])
            return bit_str + '0'
            
        
            
            