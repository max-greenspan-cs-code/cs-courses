#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 20 16:40:16 2021

@author: maxgreenspan
"""

# Problem 1:

def num_multiples(m, values):
    """inputs positive int m and list of values - returns num of ints hat are multiples of m """
    count = 0
    for x in values:
        if x % m == 0:
            count += 1
    return count

def add_stars(s):
    """inputs string and returns with stars in between adjacent characters """
    result = ''
    for x in s:
        result += x + '*'
    return result[:-1]
        
def compare(s1, s2):
    """ inputs two strings and returns the number of occurences where there are different characters """
    result = 0
    len_shorter = min(len(s1), len(s2))
    extra = abs(len(s1) - len(s2))

    for i in range(len_shorter):
        if s1[i] != s2[i]:
            result += 1
    return result + extra
        
def begins_with(prefix, wordlist):
    """ inputs a list of words and returns those that have the begin with the prefix"""
    result = []
    for i in wordlist:
        if i[:len(prefix)] == prefix:
            result += [i]
    return result
            
# Problem 2:

def log(b, n):
    count = 0
    while n > 1:
        print('dividing', n, 'by', b, 'gives', n // b)
        n //= b
        count += 1
    return count

def add_powers(m, n):
    """ inputs positive integer m and arbitrary integer n, returns the sum of m powers of n """
    count = 0
    for x in range(m):
        count += n ** x
        print(n, '**', x, '=', n ** x )
    return count