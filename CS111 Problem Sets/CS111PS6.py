#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 16:53:53 2021

@author: maxgreenspan
"""

def BUtify(s):
    """ inputs string s and returns string with capitals for letters b and u"""
    count = ''
    for i in range(len(s)):
        if s[i] == 'b' :
            count += 'B'
        elif s[i] == 'u':
            count += 'U'
        else :
            count += s[i]
    return count
    
def diff(vals1, vals2):
    """ inputs two lists of values and returns the absolute value between each list index """
    if len(vals1) > len(vals2):
        a = len(vals2)
        b = vals1[a:]
    elif len(vals2) > len(vals1):
        a = len(vals1)
        b = vals2[a:]
    else:
        a = len(vals1)
        b = []
    count = []
    for i in range(a):
        count += [abs(vals1[i] - vals2[i])]
    return count + b
    
def index(elem, seq):
    """ inputs elem and returns the index where elem first appears in seq"""
    count = 0
    for i in range(len(seq)):
        if seq[i] == elem:
            return count
        else :
            count += 1
    return -1

def square_evens(vals):
    """ inputs a list of values, returns the list with the square of all even numbers"""
    count = []
    for i in range(len(vals)):
        if vals[i] % 2 == 0:
            count += [vals[i] ** 2]
            vals[i] **= 2
        else :
            count += [vals[i]]


#  (Problem Set 6, Problem 2)
# TT Securities    


def display_menu():
    """ prints a menu of options
    """  
    print()
    print('(0) Input a new list of prices')
    print('(1) Print the current prices')
    print('(2) Find the latest price')
    print('(3) Find the average price')
    print('(4) Find the min price and its day')
    print('(5) Find the max single-day change and its day')
    print('(6) Test a threshold')
    print('(7) Your investment plan')
    print('(8) Quit')

def get_new_prices():
    """ gets a new list of prices from the user and returns it
    """
    new_price_list = eval(input('Enter a new list of prices: '))
    return new_price_list

def print_prices(prices):
    """ prints the current list of prices
        input: prices is a list of 1 or more numbers.
    """
    if len(prices) == 0:
        print('No prices have been entered.')
        return
    
    print('Day Price')
    print('--- -----')
    for i in range(len(prices)):
        print('%3d' % i, end='')
        print('%6.2f' % prices[i])

def latest_price(prices):
    """ returns the latest (i.e., last) price in a list of prices.
        input: prices is a list of 1 or more numbers.
    """
    return prices[-1]

def avg_price(vals):
    """ inputs list of prices, returns the average"""
    count = 0
    num = 0
    for x in vals:
        count += x
        num += 1
    a = count / num
    return a

def min_day(vals):
    """inputs list of values, returns the minimum values index"""
    count = 0
    current = vals[0]
    for i in range(len(vals)):
        if vals[i] < current:
            current = vals[i]
            count = i
    return count
        
def max_change_day(vals):
    """inputs list of values, returns the greatest change in one day"""
    indexs = 0
    current = 0
    for i in range(len(vals)):
        if i == 0:
            indexs = 0
        elif (vals[i] - vals[i-1]) > current:
            current = vals[i] - vals[i-1]
            indexs = i
    return indexs

def any_above(vals, limit):
    """ inputs values and a limit, returns boolean True if 
    values are greater than limit """
    for x in vals:
        if x > limit:
            return True
    return False

def find_tts(vals):
    """ finds in a list of values the greatest positive 
    difference for trading stocks and returns the day 
    to buy, day to sell, and the profit to be made """
    sell = 0
    buy = 0
    profit = 0
    r = 0
    for i in range(len(vals)):
        r += 1
        for c in range(r,len(vals)):
            if vals[c] - vals[i] > profit:
                buy = i
                sell = c
                profit = vals[c] - vals[i]
    return [buy, sell, profit]


def tts():
    """ the main user-interaction loop
    """
    prices = []

    while True:
        display_menu()
        choice = int(input('Enter your choice: '))
        print()

        if choice == 0:
            prices = get_new_prices()
        elif choice == 8:
            break
        elif choice < 8 and len(prices) == 0:
            print('You must enter some prices first.')
        elif choice == 1:
            print_prices(prices)
        elif choice == 2:
            latest = latest_price(prices)
            print('The latest price is', latest)
        elif choice == 3:
            avg = avg_price(prices)
            print('The average price is', avg)
        elif choice == 4:
            mini = min_day(prices)
            print('The min price is', prices[mini], 'on day', mini)
        elif choice == 5:
            index = max_change_day(prices)
            print('The max single-day change occurs on day', index)
            print('when the price goes from', prices[index - 1],'to', prices[index])
        elif choice == 6:
            six = int(input('Enter the threshold: '))
            if any_above(prices, six) == True:
                print('There is at least one price above', six)
            else:
                print('There are no prices above', six)
        elif choice == 7:
            seven = find_tts(prices)
            print('Buy on day', seven[0], 'at price', prices[seven[0]]) 
            print('Sell on day', seven[1], 'at price', prices[seven[1]]) 
            print('Total profit:', seven[2])
        else:
            print('Invalid choice. Please try again.')

    print('See you yesterday!')

# Problem 3:
# 2-D Lists

import random

def create_grid(num_rows, num_cols):
    """ creates and returns a 2-D list of 0s with the specified dimensions.
        inputs: num_rows and num_cols are non-negative integers
    """
    grid = []
    
    for r in range(num_rows):
        row = [0] * num_cols     
        grid += [row]

    return grid

def print_grid(grid):
    """ prints the 2-D list specified by grid in 2-D form,
        with each row on its own line.
        input: grid is a 2-D list
    """
    num_rows = len(grid)
    for r in range(num_rows):
        if r == 0:
            print('[', end='')
        else:
            print(' ', end='')
        if r < num_rows - 1:
            print(str(grid[r]) + ',')
        else:
            print(str(grid[r]) + ']')

def random_grid(num_rows, num_cols):
    """ creates and returns a 2-D list with the specified dimensions
        in which each cell is assigned a random integer from 0-9.
        inputs: num_rows and num_cols are non-negative integers
    """
    grid = create_grid(num_rows, num_cols)

    for r in range(num_rows):
        for c in range(num_cols):
            grid[r][c] = random.choice(range(10))
            
    return grid

def row_index_grid(num_rows, num_cols):
    """ creates and returns a 2-d list with specified dimensions 
    with each cell displaying the value of the index of the row """
    new_grid = create_grid(num_rows, num_cols)
    for r in range(num_rows):
        for c in range(num_cols):
            new_grid[r][c] = r
    return new_grid

def num_between(grid, n1, n2):
    """ inputs grid and two numbers, n1 and n2. returns 
    the number of numbers in the grid between n1 and n2 """
    count = 0
    for r in range(len(grid)):
        for c in range(len(grid[0])):
            if n1 <= grid[r][c] <= n2:
                count += 1
    return count


def copy(grid):
    """ creates a deep copy of a grid """
    new_grid = create_grid(len(grid), len(grid[0]))
    for r in range(len(new_grid)):
        for c in range(len(new_grid[0])):
            new_grid[r][c] = grid[r][c]
    return new_grid

def double_with_cap(grid, cap):
    """ inputs list of integers and returns double each integer
    unless it is greaer than cap, and then it instead returns cap """
    for r in range(len(grid)):
        for c in range(len(grid[0])):
            g = grid[r][c]
            if g * 2 > cap:
                grid[r][c] = cap
            else :
                grid[r][c] = g * 2

def sum_evens_in_col(grid, colnum):
    """ inputs list of integers and returns the sum of even integers in column colnum"""
    count = 0
    for r in range(len(grid)):
            if grid[r][colnum] % 2 == 0:
                count += grid[r][colnum]
    return count
    
    
    
   
                    
                


            





