# 
# ps7pr2.py - Problem Set 7, Problem 2
# Rectangle class

import math
class Rectangle:
    def __init__(self, init_width, init_height, init_unit):
        """ constructor for a Rectangle object with the specified dimensions 
            init_width and init_height, and initial x and y coordinates of 0
        """
        self.x = 0
        self.y = 0
        self.width = init_width
        self.height = init_height
        self.unit = init_unit
    
    def diagonal(self):
        """ returns square root of self """
        return math.sqrt((self.width ** 2) + (self.height ** 2))
    
    def larger_than(self, other):
        """ determines whether self or other has a larger area """
        if self.unit != other.unit:
            return False
        elif self.area() > other.area():
            return True
        else:
            return False
    
    def grow(self, dwidth, dheight):
        """ modifies the dimensions of the called Rectangle object
            (the one given by self) by adding dwidth to the current width
            and dheight to the current height.
            Note that nothing needs to be returned. The changes are
            made to the internals of the called object, and thus they 
            will still be there after the method returns!
        """
        self.width += dwidth
        self.height += dheight

    def area(self):
        """ computes and returns the area of the called Rectangle object
        """
        return self.width * self.height

    def perimeter(self):
        """ computes and returns the perimeter of the called Rectangle object
        """
        return 2*self.width + 2*self.height

    def scale(self, factor):
        """ modifies the dimensions of the called Rectangle object
            by multiplying them by the specified factor.
        """
        self.width *= factor
        self.height *= factor

    def __eq__(self, other):
        """ determines if the called Rectangle object (self) is 
            equivalent to the Rectangle object given by other
            note: the \ symbol at the end of the first line below 
            allows us to continue that line onto the next line of the file.
        """
        if self.width == other.width and \
           self.height == other.height and \
           self.unit == other.unit:
            return True
        else:
            return False

    def __repr__(self):
        """ creates and returns a string representation of the 
            called Rectangle object
        """
        return str(self.width) + ' x ' + str(self.height) + ' ' + str(self.unit)

# Problem 2:

def main():
    """ Gives input statements to be used in calcity"""
    file = input("Enter the name of data file: ")
    while True:
        city = input("City: ")
        if city == 'quit':
            break
        state = input("state abbreviation: ")
        if state == 'quit':
                break
        calcity(file, city, state)
    
def calcity(file, city, state):
    """ inputs file and returns information for given city and state """
    file = open(file, 'r')
    count = 0
    for line in file:
        line = line[:-1]
        fields = line.split(',')
        if fields[2] == city and fields[3] == state:
            count += 1
            pop = int(float(fields[4]) * 1000)
            print(fields[0] + '\t' + fields[1] + '\t' + format(pop, '10,d'))
    if count == 0:
        print('Error! No result for the city and state are found in the data file')
    file.close()
