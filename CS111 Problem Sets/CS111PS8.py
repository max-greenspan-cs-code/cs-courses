#
# ps8pr1.py  (Problem Set 8, Problem 1)
# A class to represent calendar dates       

class Date:
    """ A class that stores and manipulates dates that are
        represented by a day, month, and year.
    """

    # The constructor for the Date class.
    def __init__(self, init_month, init_day, init_year):
        """ constructor that initializes the three attributes  
            in every Date object (month, day, and year)
        """
        self.month = init_month
        self.day = init_day
        self.year = init_year

    # The function for the Date class that returns a string
    # representation of a Date object.
    def __repr__(self):
        """ This method returns a string representation for the
            object of type Date that it is called on (named self).

            ** Note that this *can* be called explicitly, but
              it more often is used implicitly via printing or evaluating.
        """
        s = '%02d/%02d/%04d' % (self.month, self.day, self.year)
        return s

    def is_leap_year(self):
        """ Returns True if the called object is
            in a leap year, and False otherwise.
        """
        if self.year % 400 == 0:
            return True
        elif self.year % 100 == 0:
            return False
        elif self.year % 4 == 0:
            return True
        return False

    def copy(self):
        """ Returns a new object with the same month, day, and year
            as the called object (self).
        """
        new_date = Date(self.month, self.day, self.year)
        return new_date


    def advance_one(self):
        """ changes the internals of self so 
        that it has been advanced forward by 1 day """
        days_in_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        
        if self.is_leap_year():
            days_in_month[2] = 29
        
        if self.day < days_in_month[self.month]:
            self.day += 1
        else:
            if self.day == days_in_month[self.month]:
                self.day = 1
                if self.month == 12:
                    self.month = 1
                    self.year += 1
                else:
                    self.month += 1
        
    def advance_n(self, n):
        """ advances self by n number of days """
        print(self)
        for i in range(n):
            self.advance_one()
            print(self)
            
    def __eq__(self, other):
        """ changes == function to reflect 
        accurately whether two dates are the same """
        if self.day != other.day:
            return False
        if self.month != other.month:
            return False
        if self.year != other.year:
            return False
        return True
        
        
    def is_before(self, other):
        """ Checks if one date is before
        another and returns boolean """
        if self.year > other.year:
            return False
        elif self.year < other.year:
            return True
        else:
            if self.month > other.month:
                return False
            elif self.month < other.month:
                return True
            else:
                if self.day > other.day:
                    return False
                elif self.day < other.day:
                    return True
                else :
                    return False
        
    def is_after(self, other):
        """ Checks if one date is after 
        another and returns boolean"""
        if self == other:
            return False
        elif self.is_before(other) == False:
            return True
        return False
        
    def days_between(self, other):
        """ returns the number of days 
        between two dates """
        x = self.copy()
        y = other.copy()
        dayz = 0
        if x == y:
            return 0
        if x.is_before(y):
            while x != y:
                dayz += 1
                x.advance_one()
            return dayz * -1
        if x.is_before(y) == False:
            while x != y:
                dayz += 1
                y.advance_one()
            return dayz
        return dayz
    
    def day_name(self):
        """ returns the day of the week
        that self falls on """
        day_names = ['Monday', 'Tuesday', 'Wednesday', 
             'Thursday', 'Friday', 'Saturday', 'Sunday']
        other = Date(11, 8, 2021)
        other.day_name = day_names[0]
        diff = self.days_between(other)
        if diff == 0:
            return other.day_name
        else:
            change = diff % 7
            return day_names[change]
        
# Problem 2:

import random

def create_dictionary(filename):
    """ inputs filename and 
    creates a dictionary of words"""
    file = open(filename, 'r')
    text = file.read()
    file.close()
    words = text.split()
    
    d = {}
    current_word = '$'
    
    for next_word in words:
        if current_word not in d:
            d[current_word] = [next_word]
        else:
            d[current_word] += [next_word]
        current_word = next_word
        if next_word[-1] in '!?.':
            current_word = '$'
    return d

def generate_text(word_dict, num_words):
    """ inputs dictionary and generates a text 
    of num_words length"""
    current_word = '$'
    for i in range(num_words):
        wordlist = word_dict[current_word]
        next_word = random.choice(wordlist)
        print(next_word, end=' ')
        current_word = next_word
        if next_word[-1] in '!?.':
            current_word = '$'
    print()
        
        