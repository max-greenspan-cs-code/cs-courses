/*
Asks for inputs of angle and height of a ladder and returns the required length of 
the ladder in feet, yards, and yards with remainder in feet

Max Greenspan - maxgspan@bu.edu
*/

import java.util.*;

public class Ladder {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //Variable Assignment Stage
        System.out.println("What is the height of the point the ladder needs to reach?");
        int height = scan.nextInt(); // Inputs given height

        System.out.println("What is the angle in degrees at which the ladder will be positioned?");
        int angle = scan.nextInt(); // Inputs given angle

        double radians = ((angle * Math.PI) / 180.0); // Converts angle into radians
        double length = ((double)height / Math.sin(radians)); // Solves for the length of the ladder

        double lengthYards = length / 3.0; // Transfers ladder length units from feet to yards
        double lengthFeetDoubleRemainder = length % 3.0; // Finds the remainder of the length of the ladder in feet 

        // Output Section
        System.out.println("The required length is:");
        System.out.println(length + " feet");
        System.out.println(lengthYards + " yards");
        System.out.println((int)lengthYards + " yards and " + lengthFeetDoubleRemainder + " feet"); 
        /* Outputs the length of the ladder in yards, 
        and the length of the latter in whole number yards with its remainder in feet
        */
    }
}
