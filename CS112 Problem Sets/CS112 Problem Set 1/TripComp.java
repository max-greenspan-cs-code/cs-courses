/*
 * TripComp.java
 * A program that allows a user to compute the cost of a trip by car,
 * based on the price of gas, the car's MPG (miles per gallon) rating,
 * and the distance of the trip.
 * 
 * CS 112 Course Staff (cs112-staff@cs.bu.edu)
 * 
 * completed by: Max Greenspan - maxgspan@bu.edu
 */ 

import java.util.*; 

public class TripComp {
    /*
     * formattedCost - takes an arbitrary real number representing a cost
     * in dollars and returns a string representation of the cost with 
     * the format "d.cc", where d is the number of dollars and cc is the
     * number of cents rounded to two places after the decimal
     */
    public static String formattedCost(double cost) {
        String costString = String.format("%.2f", cost);
        return costString;
    }
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); // Creates Scanner class for subsequent user input
    
        System.out.print("gas price in cents: ");
        int gasPrice = scan.nextInt();

        System.out.print("MPG rating of the car: ");
        int mpgRating = scan.nextInt();

        System.out.print("distance in miles: ");
        int distance = scan.nextInt();

        double cost = ((gasPrice / 100.0) * ((double)distance / mpgRating)); // Calculates total cost

        if (cost == (int)cost) {
            System.out.print("The cost of the trip is $" + (int)cost +"."); // Returns int form of Cost
        }
        else {
            System.out.print("The cost of the trip is $" + formattedCost(cost) + "."); // Returns formmatedCost
        }
        
    }
}
