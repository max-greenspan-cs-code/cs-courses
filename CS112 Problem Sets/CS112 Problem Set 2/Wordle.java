/*
 * Wordle.java 
 * 
 * An console-based implementation of a popular word-guessing game
 * 
 * starter code: Computer Science 112 staff (cs112-staff@cs.bu.edu)
 *
 * completed by:  Max Greenspan - maxgspan@bu.edu
 * 
 * Wordle! A fun Game! 
 */

import java.util.*;

public class Wordle {
    // the name of a file containing a collection of English words, one word per line
    public static final String WORD_FILE = "words.txt";

    /*
     * printWelcome - prints the message that greets the user at the beginning of the game
     */  
    public static void printWelcome() {
        System.out.println();   
        System.out.println("Welcome to Wordle!");
        System.out.println("The mystery word is a 5-letter English word.");
        System.out.println("You have 6 chances to guess it.");
        System.out.println();
    }
    
    /*
     * initWordList - creates the WordList object that will be used to select
     * the mystery work. Takes the array of strings passed into main(),
     * since that array may contain a random seed specified by the user 
     * from the command line.
     */
    public static WordList initWordList(String[] args) {
        int seed = -1;
        if (args.length > 0) {
            seed = Integer.parseInt(args[0]);
        }

        return new WordList(WORD_FILE, seed);
    }

    /*
     * readGuess - reads a single guess from the user and returns it
     * inputs:
     *   guessNum - the number of the guess (1, 2, ..., 6) that is being read
     *   console - the Scanner object that will be used to get the user's inputs
     */
    public static String readGuess(int guessNum, Scanner console) {
        String guess;
        do {
            System.out.print("guess " + guessNum + ": ");
            guess = console.next();
        } while (! isValidGuess(guess));

        return guess.toLowerCase();
    }

    /**** ADD YOUR METHODS FOR TASK 1 HERE ****/

     
    public static boolean includes(String s, char c){ // Returns boolean for if char C is included in String s
        String cString = c +"";
        
        if (s.contains(cString)) {
            return true;
        }

        else {
            return false;
        }
    }

    public static boolean isAlpha(String s){ // Returns boolean for if String s is Alphabetical
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (Character.isAlphabetic(c) == false){
                return false;
            }
        }
        return true;
    }

    public static int numOccur(char c, String s){ // Returns Int Count for number of times char c occurs in String s
        int count = 0;
        for (int i=0; i<s.length(); i++){

            char sChar = s.charAt(i);
            if (sChar == c){
            count += 1;
            }
        }
        return count;
    }

    public static int numInSamePosn(char c, String s1, String s2){ // Returns Int Count for number of times char c 
        int count = 0;                                             // is in the same position in String s1 and String s2
        for (int i=0; i<s1.length(); i++){
            char s1char = s1.charAt(i);
            char s2char = s2.charAt(i);

            if ((s1char == c) && (s2char == c)){
                count += 1;
            }
        }
        return count;
    }

    


    /*
     * TASK 2: Implement this method
     * 
     * isValidGuess -  takes an arbitrary string guess and returns true
     * if it is a valid guess for Wordle, and false otherwise
     */
    public static boolean isValidGuess(String guess) { // Returns boolean for if String guess is valid (5 letter alphabetic word)

        if (guess.length() != 5){
            System.out.println("Your guess must be 5 letters long.");
            return false;
         }

         else if (isAlpha(guess) == false){
             System.out.println("Your guess must only contain letters of the alphabet.");
             return false;
         }
         else {
             return true;
         }

    }

    

    /**** ADD YOUR METHOD FOR TASKS 3 and 5 HERE. ****/
    public static boolean processGuess(String guess, String mystery){ // processes the player's guess and gives feedback!

        String output = " ";
        int numCounter = 0;
        for (int i=0; i<5; i++){
            String iGuess = guess.substring(i,i+1); //iGuess -> "guess[i]"
            char cMystery = mystery.charAt(i); // cMystery -> 'mystery[i]'

            if (includes(iGuess, cMystery)){ // if same position and letter
                output += " " + iGuess;
            }

            else if (mystery.contains(iGuess)){ // if mystery contains "guess[i]"
                int numGuess = numOccur(guess.charAt(i), guess); // numOccur of 'guess[i]' in guess
                int numMystery = numOccur(guess.charAt(i), mystery); // numOccur of 'guess[i]' in mystery

                if (numGuess > numMystery){ // if 'guess[i]' occurs more in Guess than Mystery
                    if (numInSamePosn(guess.charAt(i), guess, mystery) == 0){ // if guess[i] is never 
                                                                            // in the same position in both words
                        if (numCounter < numMystery){
                            output += " [" + iGuess + "]"; 
                            numCounter += 1;
                        }
                        else {
                            output += " _"; 
                        }
                    }
                    else {
                        output += " _";
                    } 
                }
                else {
                    output += " [" + iGuess + "]";
                }
                } 
            else {
                output += " _";
            }
        }
        System.out.println(output);
        System.out.println();
        if  (!(output.contains("[") || output.contains("_"))){
            return true;
        }
        else {
            return false;
        }
        
    }
    
    


    


    public static void main(String[] args) {

        Scanner console = new Scanner(System.in);
        
        printWelcome();

        // Create the WordList object for the collection of possible words.
        WordList words= initWordList(args);

        // Choose one of the words as the mystery word.
        String mystery = words.getRandomWord();


        

        /*** TASK 4: Implement the rest of the main method below. ***/

        for (int i = 1; i < 7; i++){ // Runs for loop for maximum 6 times to simulate Wordle Game
            String playerGuess = readGuess(i, console); // Calls readGuess which takes a players guess and assigns it to a variable
            boolean outcome = processGuess(playerGuess, mystery); // Processes players guess and returns boolean for whether they won
            if (outcome){
                System.out.println("Congrats! You guessed it!");
                break;
                
            }
            else if (i == 6) {
                System.out.println("Sorry! Better luck next time!");
                System.out.print("The word was ");
                System.out.println(mystery + ".");
                
            }
            else ;
        }
        console.close();
        }
        }