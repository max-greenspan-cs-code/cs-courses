/*
CS112 Problem Set 3 Part 2: ArrayMethods 
Student: Max Greenspan
Email: maxgspan@bu.edu
*/


import java.util.*;

public class ArrayMethods { // A class that does numerous functions to a given array
    public static final String[] DAY_NAMES = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    public static int getDayNum(String day){ // Inputs String "Day" and returns the index of its occurence in DAY_NAMES
        if (day==null){
            throw new IllegalArgumentException();
        }
        int count = 0;
        for (int i=0; i<7; i++){
            String iDay = DAY_NAMES[i];
            count += 1;
            if (iDay.equalsIgnoreCase(day)){
                return count-1;
            }

        }
            return -1;
    }

    public static void swapNeighbors(int[] values) { // Inputs a integer array of values and swaps the "neighboring" values in the list
        if (values==null){
            throw new IllegalArgumentException();
        }
        for (int i=1; i<values.length; i += 2){
            int holdValue = values[i-1];
            values[i-1] = values[i];
            values[i] = holdValue;
        }
    }

    public static int[] copyWithCeiling(int[] values, int ceiling){ // Copies and returns an array of interger values but capped at value ceiling

        if (values==null){
            throw new IllegalArgumentException();
        }

        for (int i=0; i < values.length; i++){
            if (values[i] > ceiling){
                values[i] = ceiling;
            }
        }
        return values;
    }

    public static int mostOccur(int[] arr){ // Inputs integer array, retruns the integer that occurs most in the array

        if (arr == null) {
            throw new IllegalArgumentException();
        }

        int currentCount = 0;
        int currentNum = arr[0];
        int bestCount = 0;
        int bestNum = arr[0];

        for (int i=0; i < arr.length; i++){
            
            if (arr[i] == currentNum){
                currentCount += 1;
            }
            if (currentCount > bestCount){
                bestCount = currentCount;
                bestNum = currentNum;
            }
            if (arr[i] != currentNum){
                currentNum = arr[i];
                currentCount = 1;
            }

            }
            return bestNum;

    }

    public static int find(int[] arr1, int[] arr2){ // Returns the first position of input array 1 in input array 2

        if (arr1 == null || arr2 == null || arr1.length == 0 || arr2.length == 0) {
            throw new IllegalArgumentException();
        }

        int countPos = 0;
        int index = 0;
        for (int i=0; i<arr2.length; i++){
            if (arr1[index] == arr2[i]){
                index++;
            }
            else {
                index = 0;
            }
            if (index == arr1.length){
                return countPos - (arr1.length - 1);
            }
            countPos += 1;

        }
        return -1;

    }
    public static void main(String[] args){
        
    }
}
