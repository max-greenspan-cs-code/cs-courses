/*
 * Card.java
 * 
 * A blueprint class for objects that represent a single playing card 
 * for a game in which cards have both colors and numeric values.
 * 
 * starter code: CS 112 Staff (cs112-staff@cs.bu.edu)
 * completed by: Max Greenspan
 * Email: maxgspan@bu.edu
 */

public class Card {
    /* The smallest possible value that a Card can have. */
    public static final int MIN_VALUE = 0;
    
    /* The possible colors that a Card can have. */
    public static final String[] COLORS = {"blue", "green", "red", "yellow"};

    /* Define the third class constant here. */
    public static final int MAX_VALUE = 9;
    /* Put the rest of your class definition below. */
    public static boolean isValidColor(String color){ // Returns boolean for if input color is valid according to variable COLORS
        
        for (int i = 0; i < COLORS.length; i++){
            if (color.equals(COLORS[i])){
                return true;
            }
        }
        return false;
    }

    private String color;
    private int value;

    public void setColor(String color){ // Sets the color of color
        if (!isValidColor(color)){
            throw new IllegalArgumentException();
        }
        this.color = color;
    }

    public void setValue(int value){ // Sets the value of value
        if (value < MIN_VALUE || value > MAX_VALUE){
            throw new IllegalArgumentException();
        }
        this.value = value;
    }

    public Card(String color, int value){ // Constructor for Card object
        setColor(color);
        setValue(value);
    }
    
    public String getColor(){ // gives access to private variable color
        return this.color;
    }

    public int getValue(){ // gives access to private variable value
        return this.value;
    }

    public String toString(){ // returns string representation of card
        return this.color + " " + this.value;
    }

    public boolean matches(Card other){ // returns boolean if this.card matches other card
        if (other == null){
            return false;
        }
        if (this.getColor() == other.getColor() || this.getValue() == other.getValue()){
            return true;
        }
        else {
            return false;
        }
    }

    public boolean equals(Card other){ // returns boolean if this.card equals other card
        if (other == null){
            return false;
        }
        else if (this.getColor().equals(other.getColor()) && this.getValue() == other.getValue()){
            return true;
        }
        else {
            return false;
        }
    }

    public static void main(String[] args){
    }
    }

