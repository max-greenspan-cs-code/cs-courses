
import java.util.*;
public class ComputerPlayer extends Player{
    
    public ComputerPlayer(String name){ // Constructs a ComputerPlayer object
        super(name);
    }
    public void displayHand(){ // Displays the computer's Hand
        System.out.println(getName() + "'s hand:");
        if (getNumCards() > 1){
            System.out.println("  " + getNumCards() + " cards");
        }
        else {
            System.out.println(getNumCards() + " card");
        }
    }
    public int getPlay(Scanner console, Card topDiscard){ // Has the ComputerPlayer automatically make a move
        int bestCard = 0;
        int counter = 0;
        for (int i=0; i<getNumCards(); i++){
            if (getCard(i).matches(topDiscard)){
                if (getCard(i).getValue() > getCard(bestCard).getValue()){
                    bestCard = i;
                }
                counter++;
            }
        }
        if (counter == 0){
            return -1;
        }
        else {
            return bestCard;
        }     
    }
}
