/*
Player.Java File
Student: Max Greenspan
Email: maxgspan@bu.edu
*/

import java.util.*;
public class Player {
    private String name;
    private Card[] hand;
    private int numCards;


    public Player(String name){ // Constructs Player Object
        this.name = name;
        this.numCards = 0;
        this.hand = new Card[CardMatch.MAX_CARDS];
    }

    public String getName(){ // Accessor for name
        return this.name;
    }

    public int getNumCards(){ // Accessor for NumCards
        return this.numCards;
    }

    public String toString(){ // ToString method for class Player
        return this.name;
    }

    public void addCard(Card card){ // Adds a card to hand
        if (numCards==CardMatch.MAX_CARDS || card==null){
            throw new IllegalArgumentException();
        }
        this.hand[numCards] = card;
        numCards++;
    }

    public Card getCard(int index){ // Accessor for a card at a given index
        if (index < 0 || index > numCards-1){
            throw new IllegalArgumentException();
        }
        else
            return this.hand[index];
    }

    public int getHandValue(){ // Returns the total value of a hand
        int total = 0;
        if (this.numCards == CardMatch.MAX_CARDS){
            total = total + 20;
        }
        for (int i=0; i<numCards; i++){
            total = total + this.hand[i].getValue();
        }
        return total;

    }

    public void displayHand(){ // Prints a Player's Hand
        System.out.println(this.name + "'s " + "hand:");
        for (int i=0; i<this.numCards; i++){
           System.out.println("  " + i + ": " + getCard(i));
        }
    }

    public Card removeCard(int index){ // Removes a card from Hand
        if (index < 0 || index > this.numCards-1){
            throw new IllegalArgumentException();
        }
        Card hold = this.hand[index];
        if (index == this.numCards-1){
            this.hand[index] = null;
        }
        else {
            Card swap = this.hand[this.numCards-1];
            this.hand[this.numCards-1] = null;
            this.hand[index] = swap;
    

        }
        this.numCards-=1;
        return hold;
    }

    public int getPlay(Scanner console, Card topDiscard){ // Asks the Player to make a deicision for their turn
        while (true){
            System.out.println(this.name + ": number of card to play (-1 to draw)?");
            int input = console.nextInt();

            if (input >= -1 && input <= this.numCards){
                return input;
            }
        }
    }




}
