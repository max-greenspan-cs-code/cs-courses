public class StringRecursion {

    public static void printWithSpaces(String str){ // Recursive algorithm that prints input string with spaces
        if (str==null || str.equals("")){
            System.out.println();
            return;
        }
        if (str.length() <= 1){
            System.out.print(str.substring(0, 1) + " ");
        }
        else {
            System.out.print(str.substring(0, 1) + " ");
            printWithSpaces(str.substring(1));
            
        }
    }


    public static String reflect(String str){ // Recursive algorithm that reflects input string like a palindrome
        if (str==null || str.equals("")){
            return "";
        }
        if (str.length() <= 1){
            return str + str;
        }
        else {
            String rest = reflect(str.substring(1));
            return str.substring(0, 1) + rest + str.substring(0, 1);
        }

    }


    public static int numDiff(String str1, String str2){ // Inputs two strings and returns the integer for number of different characters
        if (str1.length() == 0 && str2.length() != 0){
            return str2.length();
        }
        else if (str2.length() == 0 && str1.length() != 0){
            return str1.length();
        }
        else if (str1.length() == 0 && str2.length() == 0){
            return 0;
        }
        int rest = numDiff(str1.substring(1), str2.substring(1));
        if ((str1.charAt(0) == str2.charAt(0)) == false){
            return rest + 1;
        }
        return rest;
    }

    public static int indexOf(char ch, String str){ // Returns the index of input char in input string. Or returns -1 if not inside
        if (str == null || str.equals("")){
            return -1;
        }
        if (str.length() == 0){
            return -1;
        }
        else if (str.substring(0,1).equals(ch + "")){
            return 0;
        }

        else {
            int rest = indexOf(ch, str.substring(1));
            if (rest == -1){
                return -1;
            }
            else {
                return rest + 1;
            }

        }
    }
}
