// Max Greenspan - maxgspan@bu.edu
// Partner: Shashank Ramachandran - sr31@bu.edu

import java.util.*;
public class Problem5 {

    public static int[] union(int[] a1, int[] a2){
        if (a1 == null || a2 == null){
            throw new IllegalArgumentException();
        }

        Sort.quickSort(a1);
        Sort.quickSort(a2);
        int union[] = new int[a1.length + a2.length];
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < a1.length && j < a2.length) { 
            if(union[1]!=0&&a1[i] < a2[j])
            if (a1[i] < a2[j]) {
                union[k]= a1[i];
                i++; k++;
            } else if(a1[i] == a2[j]){
                union[k]=a1[i];
                i++;j++;k++;
            }else {
                union[k] = a2[j];
                j++; k++;
            }
        }
        return union;



    }

    public static void main(String[] args) {
        int[] a1 = {10, 5, 7, 5, 9, 4};
        int[] a2 = {7, 5, 15, 7, 7, 9, 10};
        int[] result1 = union(a1, a2);
        System.out.println("result1: " + Arrays.toString(result1));

        int[] a3 = {0, 2, -4, 6, 10, 8};
        int[] a4 = {12, 0, -4, 8};
        int[] result2 = union(a3, a4);
        System.out.println("result2: " + Arrays.toString(result2));
    }

}