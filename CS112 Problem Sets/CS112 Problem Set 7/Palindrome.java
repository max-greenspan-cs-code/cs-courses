/*
 * Palindrome.java
 *
 * Computer Science 112
 */
   
public class Palindrome {
    public static boolean isPal(String s) {
        if (s == null){
            throw new IllegalArgumentException();
        }
        if (s.length() <= 1){
            return true;
        }
        ArrayQueue<Character> queueArr = new ArrayQueue<Character>(s.length());
        ArrayStack<Character> stackArr = new ArrayStack<Character>(s.length());
        for (int i=0; i < s.length(); i++){
            if (Character.isAlphabetic(s.charAt(i))){
                char rest = Character.toLowerCase(s.charAt(i));
                queueArr.insert(rest);
                stackArr.push(rest);
            }  
        }
        while (stackArr.isEmpty() == false){
            char q = queueArr.remove();
            char r = stackArr.pop();
            if (r != q){
                return false;
            }
            }
            return true;
        }
   
    public static void main(String[] args) {
        System.out.println("--- Testing method isPal #1 ---");
        System.out.println();

        System.out.println("(0) Testing on \"A man, a plan, a canal, Panama!\"");
        try {
            boolean results = isPal("A man, a plan, a canal, Panama!");
            boolean expected = true;
            System.out.println("actual results:");
            System.out.println(results);
            System.out.println("expected results:");
            System.out.println(expected);
            System.out.print("MATCHES EXPECTED RESULTS?: ");
            System.out.println(results == expected);
        } catch (Exception e) {
            System.out.println("INCORRECTLY THREW AN EXCEPTION: " + e);
        }
        
        System.out.println();    // include a blank line between tests
        
        /*
         * Add five more unit tests that test a variety of different
         * cases. Follow the same format that we have used above.
         */

        System.out.println("--- Testing method isPal #2 ---");
        System.out.println();

        System.out.println("(0) Testing on \"Eva, can I see bees in a cave?\"");
        try {
            boolean results = isPal("Eva, can I see bees in a cave?");
            boolean expected = true;
            System.out.println("actual results:");
            System.out.println(results);
            System.out.println("expected results:");
            System.out.println(expected);
            System.out.print("MATCHES EXPECTED RESULTS?: ");
            System.out.println(results == expected);
        } catch (Exception e) {
            System.out.println("INCORRECTLY THREW AN EXCEPTION: " + e);
        }
        
        System.out.println();    // include a blank line between tests

        System.out.println("--- Testing method isPal #3 ---");
        System.out.println();

        System.out.println("(0) Testing on \"Was it a cat I saw?\"");
        try {
            boolean results = isPal("Was it a cat I saw?");
            boolean expected = true;
            System.out.println("actual results:");
            System.out.println(results);
            System.out.println("expected results:");
            System.out.println(expected);
            System.out.print("MATCHES EXPECTED RESULTS?: ");
            System.out.println(results == expected);
        } catch (Exception e) {
            System.out.println("INCORRECTLY THREW AN EXCEPTION: " + e);
        }
        
        System.out.println();    // include a blank line between tests

        System.out.println("--- Testing method isPal #4 ---");
        System.out.println();

        System.out.println("(0) Testing on \"Sit on a potato pan, Otis\"");
        try {
            boolean results = isPal("Sit on a potato pan, Otis");
            boolean expected = true;
            System.out.println("actual results:");
            System.out.println(results);
            System.out.println("expected results:");
            System.out.println(expected);
            System.out.print("MATCHES EXPECTED RESULTS?: ");
            System.out.println(results == expected);
        } catch (Exception e) {
            System.out.println("INCORRECTLY THREW AN EXCEPTION: " + e);
        }
        
        System.out.println();    // include a blank line between tests


        System.out.println("--- Testing method isPal #5 ---");
        System.out.println();

        System.out.println("(0) Testing on \"rotator\"");
        try {
            boolean results = isPal("rotator");
            boolean expected = true;
            System.out.println("actual results:");
            System.out.println(results);
            System.out.println("expected results:");
            System.out.println(expected);
            System.out.print("MATCHES EXPECTED RESULTS?: ");
            System.out.println(results == expected);
        } catch (Exception e) {
            System.out.println("INCORRECTLY THREW AN EXCEPTION: " + e);
        }
        
        System.out.println();    // include a blank line between tests
    }
}