Hello!
I am Max Greenspan, and here is my work as a Computer Science student at Boston University.

Courses:
CS 111 - Intro to Computer Science 1:
Develops computational problem-solving skills by programming in the Python language, 
and exposes students to a variety of other topics from computer science and its applications.

CS 112 - Intro to Computer Science 2:
Covers advanced programming techniques and data structures using the Java language. 
Topics include searching and sorting, recursion, algorithm analysis, linked lists, stacks, queues, trees, and hash tables.

CS 131 - Combinatoric Structures:
It is the mathematical background course needed for much of computer science, including data
structures, algorithms, machine learning, data mining, security, cryptography, programming languages,
computational complexity, and others. It focuses especially on mathematical processes: problem solving, reasoning,
communication, and making connections, and in particular, on the skills of writing proofs 
and communicating mathematical ideas. 
